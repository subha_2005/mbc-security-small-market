Feature: Security Scenarios for Small Market

  @MBCScenariosSmall @PasswordtextfeildforSmallMarket
  Scenario: To test that password text field does not accept SQL Queries
    Given User launches the MBC Application in Browser for Small Market
    When  User enters the username in Username textbox for Small Market
    And   User enters the SQL Query as password in the Password textbox for Small Market
    And   User clicks on submit for Small Market
    Then  Verify error message for wrong Username and Password for Small Market

  @MBCScenariosSmall @SSNtextfieldforSmallMarket
  Scenario: To test that SSN textfield does not accept SQL Queries
    Given User launches the MBC Application in Browser for Small Market
    When User enters the username in Username textbox for Small Market
    And User enters the password in the Password textbox for Small Market
    And User clicks on submit for Small Market
    When User clicks on Menu for Small Market
    And User clicks on Complete Life Event for Small Market
    And User clicks on Get Started Tab for Small Market
    And User navigates to Who's covered page for Small Market
    Then User clicks on Add Dependent Button for Small Market
    And User clicks on Continue Button for Small Market
    And User enters the SQL Query in SSN textfield for Small Market
    And Verify error message when entered wrong SSN for Small Market
    And User logs out of MBC Application for Small Market

  @MBCScenariosSmall @MyInformationPageforSmallMarket
  Scenario: To test that ContactField does not accept SQL Queries
    Given User launches the MBC Application in Browser for Small Market
    When User enters the username in Username textbox for Small Market
    And User enters the password in the Password textbox for Small Market
    And User clicks on submit for Small Market
    When User clicks on Menu for Small Market
    And User clicks on Complete Life Event for Small Market
    And User clicks on Get Started Tab for Small Market
    And User navigates to Who's covered page for Small Market
    Then User navigates to My Information page for Small Market
    And User clicks on edit button MyInfoPage for Small Market
    And User enters SQL query in Mobile phone text field for Small Market
    And Verify error message for entering wrong Mobile number for Small Market
    And User logs out of MBC Application for Small Market

  @MBCScenariosSmall @CrossSiteforSmallMarket
  Scenario: To test whether an alert popup is displayed on entering the script in editable fields
    Given User launches the MBC Application in Browser for Small Market
    When User enters the username in Username textbox for Small Market
    And User enters the password in the Password textbox for Small Market
    And User clicks on submit for Small Market
    When User clicks on Menu for Small Market
    And User clicks on Profile link for Small Market
    And User clicks on edit button in Profile page for Small Market
    Then User enters the data in the editable field for Small Market
    And Verifies the alert popup for Small Market
    And User logs out of MBC Application for Small Market

  @MBCScenariosSmall @ClickJackingforSmallMarket
  Scenario: To validate if the site is vulnerable for Small Market
    Given User creates an html file for Small Market
    When User tries to open the file for Small Market

  @MBCScenariosSmall @HTTPSecurityforSmallMarket
  Scenario: To check that MBC Login page should not be accessible with http
    Given  User launches the MBC login page in Browser for Small Market
    When   User changes https to http for Small Market
    Then   User should be redirected to https login page for Small Market

  @MBCScenariosSmall @NoCookiesforSmallMarket
  Scenario: To validate that site doesnt have any cookies stored to load dashboard after logging out from the site
    Given User launches the MBC login page in Browser for Small Market
    And User enters username and password and clicks on Submit for Small Market
    When  User logsout of MBC and tries to navigate back for Small Market
    # uncomment wait for element visible password
    Then User lands back on Login page for Small Market

  @MBCScenariosSmall @WeakLockOutforSmallMarket
  Scenario: To check that the user account gets locked out on attempting invalid logins morethan a specific number of times
    Given  User launches the MBC login page in Browser for Small Market
    When   User enters invalid username and password and clicks Submit for Small Market

